import { defineComponent, onMounted } from 'vue'
import './app.less' // css 设置白色背景及安全距离
import { socketio } from './libs/socketio'

export default defineComponent({
  name: 'APP',
  setup (props, ctx) {
    //
    onMounted(() => {
      socketio.socket.on('falcon', data => {
        console.log('call:', data)
      })
    })

    // emit msg
    function handleEmitMsg () {
      socketio.push('message', 'gg')
    }
    return () => (
      <div>
        <p><button onClick={ handleEmitMsg }>emit</button></p>
      </div>
    )
  }
})
