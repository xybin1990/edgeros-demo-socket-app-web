#!/bin/bash

#定义文件夹路径
server_dir=/Users/nic/Documents/temp/socket-demo/public/
web_dir=/Users/nic/Documents/temp/socket-web

cd ${server_dir}
echo "当前路径:"
pwd

echo "*************************"

rm -rf ${server_dir}/*
echo "旧文件已删除"

cp -r ${web_dir}/dist/. ${server_dir}
echo "copy done!"
rm -rf ${web_dir}/dist
echo "临时文件已删除!"

# ==================================================

echo ">> done 操作结束! <<"
